/* eslint-disable jsx-a11y/heading-has-content */
import React, { useState, useEffect, useRef } from "react";
// import {postLead,postData,getDocDetails,getDocId} from "../../../../api";
import '../../../styles/Upsc Pathshala/UpscV2/styles.css'


function SmartForm(props) {

	const[showNext, setShowNext] = useState({
			input1: false,
			input2: false,
			input3: false,
			input4: false
		})
	
		const[showInput, setShowInput] = useState({
			input1: true,
			input2: false,
			input3: false,
			input4: false
		})
	
		// const urlParams = window.location.search.substring(1);
	
		// let parameters = urlParams.split("&").reduce((obj, str) => {
		// 	let strParts = str.split("=");
		// 	if (strParts[0] && strParts[1]) {
		// 		obj[strParts[0].replace(/\s+/g, "")] = strParts[1].trim();
		// 	}
		// 	return obj;
		// }, {});
	
		// let Today = new Date();
		// const values = useRef({
		// 	brand: props.brand,
		// 	name: "",
		// 	email: "",
		// 	phone: "",
		// 	option: "",
		// 	date: JSON.stringify(Today),
		// 	isSubmitted: "false",
		// 	variantId: props.variantId,
		// 	version_id: "eExYcq9GqCygRu5e8DCd",
		// 	parameters: urlParams
		// });
	
	
	
		const name_func = (e) => {
			const isValid = new RegExp(/^[a-zA-Z ,.'-]+$/i);
			if (e.target.value === "") {
				e.target.parentNode.nextSibling.firstChild.innerText ="*This Field Is Mandatory";
				setShowNext(prev => ({
					...prev,
					input1: false
				}))
				ref.current[e.target.name] = false
				return false
			}
			else if (!isValid.test(e.target.value)) {
				e.target.parentNode.nextSibling.firstChild.innerText = "Please Enter A Valid Name";
				setShowNext(prev => ({
					...prev,
					input1: false
				}))
				ref.current[e.target.name] = false
				return false
			}
			else {
				e.target.parentNode.nextSibling.firstChild.innerText = "";
				setShowNext(prev => ({
					...prev,
					input1: true
				}))
				ref.current[e.target.name] = true
				return true
			}
		}
		const email_func = (e) => {
			const isValid = new RegExp(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/);
			if (e.target.value === "") {
				e.target.parentNode.lastElementChild.innerText = "*This Field Is Mandatory"
				setShowNext(prev => ({
					...prev,
					input3: false
				}))
				ref.current[e.target.name] = false
				return false
			}
			else if (!isValid.test(e.target.value)) {
				e.target.parentNode.lastElementChild.innerText = "Please Enter A Valid Email"
				setShowNext(prev => ({
					...prev,
					input3: false
				}))
				ref.current[e.target.name] = false
				return false
			}
			else {
				e.target.parentNode.lastElementChild.innerText = ""
				setShowNext(prev => ({
					...prev,
					input3: true
				}))
				ref.current[e.target.name] = true
				return true
			}
		}
		const phone_func = (e) => {
			const isValid = new RegExp(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/);
			if (e.target.value === "") {
				e.target.parentNode.lastElementChild.innerText = "*This Field Is Mandatory"
				setShowNext(prev => ({
					...prev,
					input2: false
				}))
				ref.current[e.target.name] = false
				return false
			}
			else if (!isValid.test(e.target.value)) {
				e.target.parentNode.lastElementChild.innerText = "Please Enter A 10 Digit Number "
				setShowNext(prev => ({
					...prev,
					input2: false
				}))
				ref.current[e.target.name] = false
				return false
			}
			else {
				e.target.parentNode.lastElementChild.innerText = ""
				setShowNext(prev => ({
					...prev,
					input2: true
				}))
				ref.current[e.target.name] = true
				return true
			}
		}
		const dropdown_func = (e) => {
			if (e.target.value === "") {
				e.target.parentNode.lastElementChild.innerText = "*Please Select An Option"
				setShowNext(prev => ({
					...prev,
					input4: false
				}))
				ref.current[e.target.name] = false
				return false
			}
			else {
				e.target.parentNode.lastElementChild.innerText = "";
				setShowNext(prev => ({
					...prev,
					input4: true 
				}))
				ref.current[e.target.name] = true
				return true
			}
		}
	
		// const DocCreated = useRef(false)
		// const UpdateCreatedDoc = useRef(false)
		// const [DocId, setDocId] = useState(undefined)
		// const VariantInfo = useRef({})
		// const handlevalidation = async(e, validate_func) => {
		// 	validate_func(e)
		// 	let key = e.target.name
		// 	values.current = ({ ...values.current, [key]: e.target.value,...parameters })
	
		// 	if (e.target.value !== "") {
		// 		if (DocCreated.current === false) {
		// 			const fetchData = async () => {
		// 				const docRef = await getDocId(values.current);
		// 				setDocId(docRef.name.split("/").pop());
		// 				let doc = await getDocDetails(props.variantId);
		// 				if (doc.fields.hasOwnProperty("leads")) {
		// 					doc.fields["leads"].stringValue = JSON.stringify(
		// 						parseInt(doc.fields["leads"].stringValue) + 1
		// 					);
		// 				} else {
		// 					doc.fields.leads = {
		// 						stringValue: "1",
		// 					};
		// 				}
		// 				for (let key in doc.fields) {
		// 					if (doc.fields.hasOwnProperty(key)) {
		// 						doc.fields[key] = doc.fields[key].stringValue;
		// 					}
		// 				}
		// 				VariantInfo.current = { ...doc.fields };
		// 				await postData("variants", props.variantId, VariantInfo.current);
		// 			};
		// 			fetchData();
		// 			DocCreated.current = true
		// 			UpdateCreatedDoc.current = true
		// 		}
		// 	}
		// 	if (UpdateCreatedDoc.current === true) {
		// 		if (DocId !== undefined) {
		// 			await postData("leadsV2",DocId, values.current)
		// 		}
		// 	}
	
		// }
	
	
		// let common_input_class = document.getElementsByClassName('common_input_class')
		// const ref = useRef({})
		// useEffect(() => {
		// 	for (let i = 0; i < common_input_class.length; i++) {
		// 		ref.current = ({ ...ref.current, [common_input_class[i].name]: false })
		// 	}
		// }, [common_input_class])
	
	
		// let isformValid
		// const onSubmitValidator = async () => {
		// 	console.log(ref.current, "ref");
		// 	for (let key in ref.current) {
		// 		if (ref.current[key] === false || common_input_class.length !== Object.keys(ref.current).length) {
		// 			isformValid = false
		// 			delete ref.current.undefined
		// 			break
		// 		}
		// 		else {
		// 			isformValid = true
		// 		}
		// 	}
		// }
	
		// const submitHandler = async(e) => {
		// 	e.preventDefault()
		// 	onSubmitValidator();
		// 	console.log(isformValid);
		// 	if (!isformValid) {
		// 		alert("Please Fill All The Fields Correctly")
		// 	}
		// 	else {
		// 		// db.collection("leadsV2").doc(DocId).set({ ...values.current, isSubmitted: true })
		// 		// db.collection("variants").doc(props.variantId).set({
		// 		// 	...VariantInfo.current,
		// 		// 	isSubmitted: VariantInfo.current.isSubmitted + 1
		// 		// })
		// 		// console.log("success")
		// 		// axios
		// 		// 	.post("https://api.ufaber.com/api/sales/fetch-lead-new/", {
		// 		// 		...values.current,
		// 		// 	})
		// 		// 	.then(
		// 		// 		(response) =>
		// 		// 		(window.location.href =
		// 		// 			"https://upscpathshala.com/online/bestupsccoachingsp/thanks/")
		// 		// 	);
		// 		values.current.isSubmitted = "true";
		// 		await postData("leadsV2", DocId, { ...values.current });
		// 		VariantInfo.current.isSubmitted = JSON.stringify(
		// 			parseInt(VariantInfo.current.isSubmitted.stringValue) + 1
		// 		);
		// 		await postData("variants", props.variantId, { ...VariantInfo.current });
		// 		await postLead(
		// 			{ ...values.current, isSubmitted: true },
		// 			"https://upscpathshala.com/online/bestupsccoachingsp/thanks/"
		// 		);
		// 	}
		// }
	
		function inputNextHandlerClick(e){
			
			if(showInput.input1){
				setShowInput({
					input1: false,
					input2: true,
					input3: false,
					input4: false
				})
			} else if(showInput.input2){
				setShowInput({
					input1: false,
					input2: false,
					input3: true,
					input4: false
				})
			} else if(showInput.input3){
				setShowInput({
					input1: false,
					input2: false,
					input3: false,
					input4: true
				})
			} else if(showInput.input4){
				submitHandler(e)
			} 
		}
	
		function inputNextHandlerKey(e) {
			
			if(e.key === "Enter" && showInput.input1){
				setShowInput({
					input1: false,
					input2: true,
					input3: false,
					input4: false
				})
			} else if(e.key === "Enter" && showInput.input2){
				setShowInput({
					input1: false,
					input2: false,
					input3: true,
					input4: false
				})
			} else if(e.key === "Enter" && showInput.input3){
				setShowInput({
					input1: false,
					input2: false,
					input3: false,
					input4: true
				})
			} else if(e.key === "Enter" && showInput.input4){
				submitHandler(e)
			}
			
		}

	console.log(props.data[1].attributes.Form.FormDescription, "--------------------")

	return (
		<div>
			{props.data.map(({ id, attributes }) => {

				if (attributes.VariantID === props.variantId) {

					return (
							<>
								<div className="outer-right-box">
									<div className="brand-info-box">
										<img className="image-fuild" src="https://di2c09dj6ldav.cloudfront.net/upsc/no1coaching/asset/img/logo.png" alt="" loading="lazy" />
										<p>{attributes.Form.FormDescription}</p>
									</div>

									<div className="main-form">
										<form autoComplete="off" action="#">
											<div className="form-input1" style={{ display: showInput.input1 === true ? "block" : "none" }}> 
												<h4>{attributes.Form.FormInputs.NameLabel}</h4>
												
												<input 
													type="text"
													name="name"
													id="fullName"
													placeholder="Type your answer here..."
													className="common_input_class"
													onChange={(e) => {
														handlevalidation(e, name_func);
													}}
													onBlur={(e) => {
														handlevalidation(e, name_func);
													}}
													onKeyDown={inputNextHandlerKey}
													required
													isvalid="false"
												/> 
												<span onClick={inputNextHandlerClick}  id="inputNext" style={{ visibility: showNext.input1 === false ? "" : "visible", opacity: "1" }}>
													<i className="fa fa-arrow-right" aria-hidden="true" id="arrow1"></i>
												</span>
												<div className="row">
													<div className="col-6">
														<h6 id="phoneError" style={{color: "white !important", fontSize: "10px" , margin: "8px 0 0", padding: "0px", display: "block"}}></h6>
													</div>
												</div>
												<div className="col-6">
													<div className="row" style={{float: "right", margin: "0px" ,padding: "0px"}}>
														<h5 id="indicator" ><u style={{cursor: "pointer"}}>1</u></h5>
														<h5 id="indicatorGrey">2</h5>
														<h5 id="indicatorGrey">3</h5>
														<h5 id="indicatorGrey">4</h5>
													</div>
												</div>
											</div>

											<div className="form-input2" style={{ display: showInput.input2 === true ? "block" : "none" }}>
												<h4 style={{fontSize: "16px"}}>What is your mobile number?</h4>
												<h4 style={{fontSize: "16px"}}>{attributes.Form.FormInputs.MobileLabel}</h4>
												<h5 style={{color: "#B29DD6", fontSize: "10px", textAlign: "left", margin: "0 0 0 2px", height: "2px", }}>{attributes.Form.FormInputs.MobileSubLabel}</h5>
												<input  
													name="phone"  
													placeholder="Type here..." 
													type="tel"
													id="phoneNumber"
													className="common_input_class"
													onChange={(e) => {
														handlevalidation(e, phone_func);
													}}
													onBlur={(e) => {
														handlevalidation(e, phone_func);
													}}
													onKeyDown={inputNextHandlerKey}
													required
													isvalid="false"
												/>

												<span onClick={inputNextHandlerClick} id="inputNext2" style={{ visibility: showNext.input2 === false ? "" : "visible", opacity: "1" }}>
													<i className="fa fa-arrow-right" aria-hidden="true" id="arrow2"></i>
												</span>

												<div className="">
													<div className="col-6">
														<h6 id="phoneError" style={{color: "white !important", fontSize: "10px" , margin: "8px 0 0", padding: "0px", display: "block"}}></h6>
													</div>
												</div>
												<div className="col-6">
													<div className="row" style={{float: "right", margin: "0px" ,padding: "0px"}}>
														<h5 id="indicator" onclick="redirect1()" style={{cursor: "pointer"}}>1</h5>
														<h5 id="indicator"><u style={{cursor: "pointer"}}>2</u></h5>
														<h5 id="indicatorGrey">3</h5>
														<h5 id="indicatorGrey">4</h5>
													</div>
												</div>
												
											</div>

											<div className="form-input3" style={{ display: showInput.input3 === true ? "block" : "none" }}>
												<h4 style={{fontSize: "16px"}}>{attributes.Form.FormInputs.EmailLabel}</h4>
												<h5 style={{color: "#B29DD6", fontSize: "10px", textAlign: "left", margin: "0 0 0 2px", height: "2px"}}>{attributes.Form.FormInputs.EmailSubLabel}</h5>
												<input  
													placeholder="Type here..."  
													type="email"
													name="email"
													id="emailID"
													className="common_input_class"
													onChange={(e) => {
														handlevalidation(e, email_func);
													}}
													onBlur={(e) => {
														handlevalidation(e, email_func);
													}}
													onKeyDown={inputNextHandlerKey}
													required
													isvalid="false"
												/>
												
												<span onClick={inputNextHandlerClick} id="inputNext3" style={{ visibility: showNext.input3 === false ? "" : "visible", opacity: "1" }}>
													<i className="fa fa-arrow-right" aria-hidden="true"></i>
												</span>

												<div className="row">
													<div className="col-6">
														<h6 id="phoneError" style={{color: "white !important", fontSize: "10px" , margin: "8px 0 0", padding: "0px", display: "block"}}></h6>
													</div>
												</div>
												<div className="col-6">
													<div className="row" style={{float: "right", margin: "0px" ,padding: "0px"}}>
														<h5 id="indicator" onclick="redirect1()" style={{cursor: "pointer"}}>1</h5>
														<h5 id="indicator" onclick="redirect2()" style={{cursor: "pointer"}}>2</h5>
														<h5 id="indicator"><u style={{cursor: "pointer"}}>3</u></h5>
														<h5 id="indicatorGrey">4</h5>
													</div>
												</div>
											</div>

								
											<div className="form-input4" style={{ display: showInput.input4 === true ? "block" : "none" }}>
												<h4 style={{fontSize: "15px"}}>{attributes.Form.FormInputs.DropdownLabel}</h4>
												<h5 style={{color: "#6C7EF2", fontSize: "10px", textAlign: "left", margin: "0 0 0 2px", height: "2px"}}></h5>
												<select 
													id="drop_val"
													name="option"
													className="common_input_class"
													onChange={(e) => {
														handlevalidation(e, dropdown_func);
													}}
													onBlur={(e) => {
														handlevalidation(e, dropdown_func);
													}}
													required
													type="dropdown"
													isvalid="false"
												>
													<option value="">Select Answer</option>
													{attributes.Form.FormInputs.DropdownOption1 ?
														<option value={attributes.Form.FormInputs.DropdownOption1}>{attributes.Form.FormInputs.DropdownOption1}</option>
													: null}
													{attributes.Form.FormInputs.DropdownOption2 ?
														<option value={attributes.Form.FormInputs.DropdownOption2}>{attributes.Form.FormInputs.DropdownOption2}</option>
													: null}
													{attributes.Form.FormInputs.DropdownOption3 ?
														<option value={attributes.Form.FormInputs.DropdownOption3}>{attributes.Form.FormInputs.DropdownOption3}</option>
													: null}
													{attributes.Form.FormInputs.DropdownOption4 ?
														<option value={attributes.Form.FormInputs.DropdownOption4}>{attributes.Form.FormInputs.DropdownOption4}</option>
													: null}
												</select>

												<span id="inputNext4" onClick={inputNextHandlerClick} style={{ visibility: showNext.input4 === false ? "" : "visible", opacity: "1" }}>
													<i className="fa fa-arrow-right" aria-hidden="true"></i>
												</span>
												<div className="row" style={{float: "right", margin: "0px", padding: "0px"}}>
													<h5 id="indicator" onclick="redirect1()" style={{cursor: "pointer"}}>1</h5>
													<h5 id="indicator" onclick="redirect2()" style={{cursor: "pointer"}}>2</h5>
													<h5 id="indicator" onclick="redirect3()" style={{cursor: "pointer"}}>3</h5>
													<h5 id="indicator"><u>4</u></h5>
												</div>

											</div>
											
										</form>
									</div>

								</div>

							</>
						)
				}
				return null
			})}
		</div>
	)
}



export default SmartForm;
