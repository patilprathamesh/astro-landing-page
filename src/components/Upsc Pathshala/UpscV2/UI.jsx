import React, {useState, useEffect} from "react";
// import Carousel from 'react-elastic-carousel';
import '../../../styles/Upsc Pathshala/UpscV2/styles.css'


function UI(props){

	// const [uiVisible, setuiVisible] = useState(true)

	const [screenSize, getDimension] = useState(500);

	useEffect(() => {
		window.addEventListener('resize', () => {
			getDimension(window.innerWidth)
		});

		return(() => {
			window.removeEventListener('resize', () => {
				getDimension(window.innerWidth)
			});
	})
	}, [screenSize])


	const formHandler = (e) => {
		// setuiVisible(false)
		console.log("cved")
		props.setFormVisible(true)
	}

	return (
		<div>
			{props.data.map(({ id, attributes }) => {

				if (attributes.VariantID === props.variantId) {

					return (
							<>
								<div className="left-upper-box" >
									{(screenSize.dynamicWidth < 768) ? 
										<img className="left-upper-box__img-logo" src={attributes.UI.UpperUI.MobileLogo.data.attributes.url} width={375} height={309} alt="" loading="lazy"/>
									: null} 

									<h3>
										{attributes.UI.UpperUI.BrandHeading}
									</h3>
									<p>
										{attributes.UI.UpperUI.BrandDescription}
									</p>

									{/* <Carousel
										className="owl-theme"
										items={1}
										autoplay={true}
										showArrows={false}
										dots
										loop
										stagePadding={0}> */}
									
										<div className="parent-left">
											<img style={{width: "100%"}}
												width={375} 
												height={309}
												src={attributes.UI.UpperUI.Carousel1Image.data.attributes.url}
												alt=""
											/>
										</div>
						
										<div className="parent-left">
											<img style={{width: "100%"}}
												width={375} 
												height={309}
												src={attributes.UI.UpperUI.Carousel2Image.data.attributes.url}
												alt=""
											/>
										</div>	
									
									{/* </Carousel> */}
								</div>

								<div className="left-middle-box">
									<div className="upper-container">
										<p className="top-text">
											{attributes.UI.MiddleUI.CollageHeading}
										</p>
										<img src={attributes.UI.MiddleUI.CollageImage.data.attributes.url} width={375} height={107} alt="" loading="lazy" />
										<p className="bottom-text">
											{attributes.UI.MiddleUI.CollageDescription}
										</p>
									</div>
									<hr />
									<div className="lower-container">
										<h4>
											{attributes.UI.BottomUI.CarouselHeading}
										</h4>
										{/* <Carousel
											className="owl-theme"
											items={1}
											autoplay={true}
											showArrows={false}
											dots
											loop
											stagePadding={0}> */}
										
											<div className="parent-left">
												{(screenSize.dynamicWidth > 768) ? 
													<img style={{width: "100%"}}
													src={attributes.UI.BottomUI.Carousel1Desktop.data.attributes.url}
													alt="" />
												: 
													<img
													width={228}
													height={445}
													src={attributes.UI.BottomUI.Carousel1Mobile.data.attributes.url}
													alt="" />
												} 
												<p className="carousel-text">{attributes.UI.BottomUI.Carousel1Description}</p>
											</div>
							
											<div className="parent-left">
												{(screenSize.dynamicWidth > 768) ? 
													<img style={{width: "100%"}}
													src={attributes.UI.BottomUI.Carousel2Desktop.data.attributes.url}
													alt="" />
												: 
													<img 
													width={228}
													height={445}
													src={attributes.UI.BottomUI.Carousel2Mobile.data.attributes.url}
													alt="" />
												} 
												<p className="carousel-text">{attributes.UI.BottomUI.Carousel2Description}</p>
											</div>	

											<div className="parent-left">
												{(screenSize.dynamicWidth > 768) ? 
													<img style={{width: "100%"}}
													src={attributes.UI.BottomUI.Carousel3Desktop.data.attributes.url}
													alt="" />
												: 
													<img 
													width={228}
													height={445}
													src={attributes.UI.BottomUI.Carousel3Mobile.data.attributes.url}
													alt="" />
												} 
												<p className="carousel-text">{attributes.UI.BottomUI.Carousel3Description}</p>
											</div>

											<div className="parent-left">
												{(screenSize.dynamicWidth > 768) ? 
													<img style={{width: "100%"}}
													src={attributes.UI.BottomUI.Carousel4Desktop.data.attributes.url}
													alt="" />
												: 
													<img 
													width={228}
													height={445}
													src={attributes.UI.BottomUI.Carousel4Mobile.data.attributes.url}
													alt="" />
												} 
												<p className="carousel-text">{attributes.UI.BottomUI.Carousel4Description}</p>
											</div>

											<div className="parent-left">
												{(screenSize.dynamicWidth > 768) ? 
													<img style={{width: "100%"}}
													src={attributes.UI.BottomUI.Carousel5Desktop.data.attributes.url}
													alt="" />
												: 
													<img 
													width={228}
													height={445}
													src={attributes.UI.BottomUI.Carousel5Mobile.data.attributes.url}
													alt="" />
												} 
												<p className="carousel-text">{attributes.UI.BottomUI.Carousel5Description}</p>
											</div>
										
										{/* </Carousel> */}
									</div>
								</div>

								<button className="btn-form-open" onClick={formHandler}>
									{attributes.UI.Footer.MobileFormButton1}
									<i className="fa fa-angle-right" aria-hidden="true"></i>
									<div style={{fontSize: "12px"}}>{attributes.UI.Footer.MobileFormButton2}</div>
								</button>

								<div className="footer-content">
									<p>{attributes.UI.Footer.DesktopCopwrightText}</p>
								</div>
								
							</>
						)
				}
				return null
			})}
		</div>
	)

	
}

export default UI;
