
import UI from './UI.jsx';
import SmartForm from './SmartForm.jsx';
import '../../../styles/Upsc Pathshala/UpscV2/styles.css'
import React, {useState, useEffect} from 'react';


const Usv2 = (props) => {


   	const [formVisible, setFormVisible]	= useState(false)
	
	const [screenSize, getDimension] = useState(500);
	  
	useEffect(() => {
		window.addEventListener('resize', () => {
            getDimension(window.innerWidth)
        });
		
		return(() => {
			window.removeEventListener('resize', () => {
                getDimension(window.innerWidth)
            });
		})
	}, [screenSize])

  return (
		<>
			<div className="mainDiv">
				{!(formVisible) ?
				<div className="leftDiv">
					<UI 
						variantId = "7Z8ROT5Vu4ysUrGrgOcc" 
						data={props.data}
						// title={props.title}
						// brand={props.element.brand}
						setFormVisible={setFormVisible}
					/>
				</div>
				: null}
				{/* <SmartForm variantId = "7Z8ROT5Vu4ysUrGrgOcc" data={props.data}/> */}
				{((screenSize > 768) || formVisible) ?
					<div className="rightDiv">
						<SmartForm style={{ display: formVisible ? "flex" : "none"}}
							variantId = "7Z8ROT5Vu4ysUrGrgOcc"
							data={props.data}
							// title={props.title}
							// brand={props.element.brand}
						/>
					</div>
				: null }
			</div>
		</>
	);
}


export default Usv2;
